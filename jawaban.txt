1 Membuat Database

create database myshop;

2 Membuat Table

Table Users
create table users( id int(10) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

Table Categories
create table categories( id int(10) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );

Table Items
create table Items( id int(10) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price int(10), stock int(10), category_id int(10), FOREIGN KEY(category_id) REFERENCES categories(id) );

3 Memasukan Data

Insert Data Users
insert into users(name, email, password) VALUES("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

Insert Data Categories
insert into categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");

Insert Data Items
insert into items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dari merek sumsang", "4000000", 100, 1), ("Uniklooh", "baju keren dari brand ternama", "500000", 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", 10, 1);

4 Mengambil Data

a. Mengambil Data Users kecuali Password
select id, name, email FROM users;

b. Mengambil Data Items
a) select * FROM items WHERE price > 1000000;
b) select * FROM items WHERE name LIKE "watch";

c. Menampilkan Data Items Join dengan Kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM items INNER JOIN categories ON items.category_id = categories.name;

5 Mengubah Data dari Database
update items set price=2500000 WHERE id = 1;